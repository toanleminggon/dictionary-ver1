/**
 * class DictionaryCommendline
 * @author Le Khanh Toan
 * @version 1.0
 */
public class DictionaryCommendline {
    /**
     *
     * @param dict
     */
    public void showAllWords(Dictionary dict) {
        int size = dict.arrayWords.size();
        System.out.printf("STT\t|English\t\t\t|Vietnamese\n");
        for (int i = 0; i < size; i++) {
            System.out.printf("%d\t|%s\t\t\t\t|%s\n", i + 1, dict.arrayWords.get(i).word_in,
                    dict.arrayWords.get(i).word_out);
        }

    }

    /**
     *
     * @param management
     * @param dict
     */
    public void dictionaryBasic(DictionaryManagement management, Dictionary dict) {

        dict = management.insertFromCommandline();
        this.showAllWords(dict);
    }
}
