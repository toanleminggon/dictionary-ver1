/**
 * class Test
 * @author Le Khanh Toan
 * @version 1.0
 *
 */
public class Test{
    public static void main(String[] args){
        DictionaryManagement management = new DictionaryManagement();
        Dictionary dict = new Dictionary();
        DictionaryCommendline dc = new DictionaryCommendline();
        dc.dictionaryBasic(management, dict);
    }
}