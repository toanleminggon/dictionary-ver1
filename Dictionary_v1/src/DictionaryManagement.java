import java.util.Scanner;

/**
 * class DictionaryManagement de quan li app
 * @author Le Khanh Toan
 * @version 1.0
 */
public class DictionaryManagement{
    public DictionaryManagement(){}

    /**
     * chèn dữ liệu vào từ command line
     * @return dict: Dictionary
     */
    public Dictionary insertFromCommandline(){
        Dictionary dict = new Dictionary();
        int number;
        Scanner sc = new Scanner(System.in);
        System.out.println("Ban hay nhap so luong tu:");
        String engString, vieString;
        number = sc.nextInt();
        engString = sc.nextLine();

        for(int i = 0; i<number; i++){

            System.out.println("Nhap tu tieng anh:");
            engString = sc.nextLine();
            System.out.println("Nhap nghia tieng viet:");
            vieString = sc.nextLine();
            Word word = new Word(engString, vieString);

            dict.addWordToDict(word);
        }

        return dict;


    }


}