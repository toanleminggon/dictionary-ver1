import java.util.ArrayList;

/**
 * class Dictionary
 * @author Le Khanh Toan
 * @version 1.0
 */
public class Dictionary {

    public ArrayList<Word> arrayWords = new ArrayList<Word>();

    /**
     * Contrustor
     */
    public  Dictionary(){

    }

    /**
     * Them word vao Dictionary
     * @param word
     */
    public void addWordToDict(Word word) {
        arrayWords.add(word);
    }
}
