/**
 * class Word gom tu Tieng Anh va Tieng Viet
 * @author Le Khanh Toan
 * @version 1.0
 */
public class Word {
    public String word_in, word_out;

    /**
     * Constructor
     */
    public Word(){

    }

    /**
     * Constructor
     * @param word_in
     * @param word_out
     */
    public Word(String word_in, String word_out){
        this.word_in = word_in;
        this.word_out = word_out;
    }

}
