import java.io.Serializable;
/**
 * class Word
 * @author Le Khanh Toan
 * @version 3.0
 */
public class Word implements Serializable {
    // Serializable chuyển đổi các đối tượng thành stream.
    protected String word_in;
    protected String word_out;
    // Phương thức get-set
    public String getWord_target() {
        return word_in;
    }

    public void setWord_target(String word_in) {
        this.word_in = word_in;
    }

    public String getWord_explain() {
        return word_out;
    }

    public void setWord_explain(String word_out) {
        this.word_out = word_out;
    }
}
